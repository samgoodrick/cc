#pragma once

#include <cc/print.hpp>

namespace cc
{
  class node;

  /// A facilitiy for implementing AST dumpers.
  ///
  /// \todo Move indentation into the pretty printer?
  class dumper : public printer
  {
  public:
    dumper(std::ostream& os)
      : printer(os), nesting()
    { }

    using printer::print;

    void print_indentation();

    void indent() { ++nesting; }
    void undent() { --nesting; }

  protected:
    struct dump_guard
    {
      dump_guard(dumper& d, const node* n, const char* str, bool nl = true);
      ~dump_guard();

      dumper& d;
      bool nl;
    };

  protected:
    int nesting;
  };

} // namespace cc
