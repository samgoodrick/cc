#pragma once

#include <cstdint>
#include <iosfwd>
#include <utility>

namespace cc
{
  class file;

  /// An encoded source code location.
  ///
  /// The value of a location is an index into the cumulative input text.
  class location 
  {
  public:
    location()
      : index(-1)
    { }

    explicit location(std::uint32_t n)
      : index(n)
    { }

    /// Converts to true when the location is valid.
    explicit operator bool() const { return is_valid(); }

    /// Returns true when the location is not valid.
    bool is_invalid() const { return index == -1; }

    /// Returns true when the location is valid.
    bool is_valid() const { return index != -1; }

    /// Returns the underlying index.
    std::uint32_t get_index() const { return index; }

    /// Returns a location n characters after (or before if n < 0) this one.
    location get_next(int n) const { return location(index + n); }

    // Equality
    friend bool operator==(location a, location b) { return a.index == b.index; }
    friend bool operator!=(location a, location b) { return a.index != b.index; }

    // Ordering
    friend bool operator<(location a, location b) { return a.index < b.index; }
    friend bool operator>(location a, location b) { return a.index > b.index; }
    friend bool operator<=(location a, location b) { return a.index <= b.index; }
    friend bool operator>=(location a, location b) { return a.index >= b.index; }

  private:
    std::uint32_t index;
  };

  /// A span of text delimited by a pair of locations.
  class span : std::pair<location, location>
  {
  public:
    span() = default;
    
    span(location loc)
      : std::pair<location, location>(loc, loc)
    { }

    span(location s, location e)
      : std::pair<location, location>(s, e)
    { }

    /// Converts to true when the start end end locations are invalid.
    explicit operator bool() const { return first && second; }

    /// Returns the start location.
    location get_start() const { return first; }

    /// Returns the end location.
    location get_end() const { return second; }
  };

// -------------------------------------------------------------------------- //
// Resolved locations

  /// The 1-based line and column of a location.
  struct coordinates
  {
    /// True if the coordinate is past the end of the file.
    bool is_eof() const { return line == 0 && column == 0; }
    
    unsigned line;
    unsigned column;
  };

  /// The resolved position of a source location. Note that line and column
  struct full_location
  {
    full_location(const file& f, coordinates c)
      : source(f), coords(c)
    { }

    const file& source;
    coordinates coords;
  };

  /// The resolved position of a span of text.
  struct full_span
  {
    full_span(const file& f, coordinates s, coordinates e)
      : source(f), start(s), end(e)
    { }

    const file& source;
    coordinates start;
    coordinates end;
  };

  std::ostream& operator<<(std::ostream& os, coordinates coords);
  std::ostream& operator<<(std::ostream& os, full_location loc);

} // namespace cc

