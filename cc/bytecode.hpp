#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace cc
{
  /// A fundamental unit of strage.
  using byte = unsigned char;

  /// A sequence of bytes.
  using byte_seq = std::vector<byte>;

  /// Enables writing to a sequence of bytes.
  ///
  /// \todo Serialize integers in network byte order.
  class byte_ostream
  {
  public:
    byte_ostream(byte_seq& bytes)
      : bytes(bytes)
    {
      bytes.reserve(4096);
    }

    void write_bytes(const byte_seq& bs);

    void write_bool(bool b);
    
    void write_int(std::uint64_t n);
    void write_int8(std::uint8_t n);
    void write_int16(std::uint16_t n);
    void write_int32(std::uint32_t n);
    void write_int64(std::uint64_t n);

    void write_string(const char* str, std::size_t len);
    void write_string(const std::string& str);

  private:
    byte_seq& bytes;
  };

  /// Enables reading from a byte stream.
  ///
  /// \todo Allow this to operate on a byte range so we can avoid
  /// copies during serialization.
  class byte_istream
  {
  public:
    byte_istream(byte_seq& bytes)
      : bytes(bytes), pos(0)
    { }

    bool read_bool();

    std::uint64_t read_int();
    std::uint8_t read_int8();
    std::uint16_t read_int16();
    std::uint32_t read_int32();
    std::uint64_t read_int64();

    std::string read_string();

  private:
    byte_seq& bytes;
    std::size_t pos;
  };
  
} // namespace cc