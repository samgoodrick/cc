#include "file.hpp"
#include "diagnostics.hpp"

#include <cassert>
#include <iterator>
#include <iostream>
#include <fstream>

namespace cc
{
  // Perform a first-pass scan of a file to construct a line map. We could do
  // this during lexing, but it makes the lexer cleaner to do build this
  // separately.
  static void
  build_line_map(const std::string& s, line_map& map, line_seq& subs)
  {
    std::size_t start = 0;
    std::size_t i = 0;
    while (i < s.size()) {
      if (s[i] == '\n') {
        map.emplace(location(i), map.size());
        subs.emplace_back(start, i - start);
        start = i + 1;
      }
      ++i;
    }
    map.emplace(location(i), map.size());
    subs.emplace_back(start, i - start);
  }

  using isb_iterator = std::istreambuf_iterator<char>;

  file::file(std::size_t n, const std::string& p, std::istream& is)
    : offset(n), path(p), text(isb_iterator(is), isb_iterator())
  { 
    build_line_map(text, lines, subs);
  }

  file::file(std::size_t n, const std::string& p, const std::string& s)
    : offset(n), path(p), text(s)
  { 
    build_line_map(text, lines, subs);
  }

  std::string
  file::get_line(int n) const
  {
    assert((0 <= n && n < lines.size()) && "invalid line number");
    return text.substr(subs[n].first, subs[n].second);
  }

  coordinates
  file::resolve_location(location loc) const
  {
    auto line = lines.lower_bound(loc);
    if (line != lines.begin()) {
      auto prev = std::prev(line);
      return {line->second + 1, loc.get_index() - prev->first.get_index()};
    }
    else {
      return {1, loc.get_index() + 1};
    }
  }

} // namespace cc

