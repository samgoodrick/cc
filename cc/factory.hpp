#pragma once

#include <list>

namespace cc
{

  /// Creates objects of a particular type. All objects are destroyed when
  /// the factory goes out of scope.
  template<typename T>
  class basic_factory : private std::list<T>
  {
  public:
    template<typename... Args>
    T* make(Args&&... args)
    {
      this->emplace_back(std::forward<Args>(args)...);
      return &this->back();
    }
  };

} // namespace cc
