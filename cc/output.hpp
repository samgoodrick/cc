#pragma once

#include <iosfwd>

namespace cc
{
  /// Represents information about an outpout stream.
  class output_device
  {
  public:
    output_device(std::ostream& os);

    /// Returns the output stream.
    std::ostream& get_stream() const { return os; }

    /// Returns true if the stream is attached to a terminal.
    bool is_terminal() const { return term; }

    /// Returns true if output supports color. Color output is supported
    /// by default when the stream is attached to a terminal.
    bool use_color() const { return color; }

    /// Explicitly enable or disable color. If the output is not a terminal,
    /// or if the outpout format is not plain text, escape codes may cause
    /// rendering issues.
    void enable_color(bool b) { color = b; }

  protected:
    /// The underlying output stream.
    std::ostream& os;

    /// True if emitting to a terminal.
    bool term;
    
    /// True the output device supports
    bool color;
  };

} // namespace cc
