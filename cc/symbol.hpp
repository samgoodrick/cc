#pragma once

#include <string>
#include <unordered_set>

namespace cc
{
  /// A unique sequence of characters.
  using symbol = const std::string;


  /// A table of unique symbols.
  class symbol_table : std::unordered_set<std::string>
  {
  public:
    /// Returns a unique string pointer for the value of `s`.
    symbol* get(const char* s) { return &*insert(s).first; }
    
    /// Returns a unique string pointer for the value of `s`.
    symbol* get(const std::string& s) { return &*insert(s).first; }
  };

} // namespace cc
