#include "input.hpp"
#include "diagnostics.hpp"

#include <cassert>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>

namespace cc
{
  const file&
  input_manager::add_file(const std::string& path)
  {
    // Compute the starting offset for the new file. Adjust the offset by
    // one so that we can detect ends-of-file.
    std::size_t off;
    if (input.empty())
      off = 0;
    else
      off = input.back().get_top_offset();
    
    // Open the file for reading.
    std::ifstream ifs(path);
    if (!ifs) {
      std::stringstream ss;
      ss << "cannot open '" << path << "' for reading";
      throw input_error(ss.str());
    }

    // Add the file.
    input.emplace_back(off, path, ifs);
    return input.back();
  }

  const file&
  input_manager::get_file(location loc) const
  {
    assert(loc.is_valid() && "invalid location");
    auto iter = std::find_if(input.begin(), input.end(), [loc](const file& f) {
      return loc.get_index() < f.get_top_offset();
    });
    assert(iter != input.end() && "location outside of input");
    return *iter;
  }

  full_location
  input_manager::resolve_location(location loc) const
  {
    const file& f = get_file(loc);
    coordinates coords = f.resolve_location(loc);
    return {f, coords};
  }

} // namespace cc

